<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Poll>
 */
class PollFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->realTextBetween($minNbChars = 15, $maxNbChars = 150, $indexSize = 1),
            'subject' => fake()->realTextBetween($minNbChars = 15, $maxNbChars = 150, $indexSize = 1),
        ];
    }
}
