<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Answer>
 */
class AnswerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'proposition' => fake()->realTextBetween($minNbChars = 160, $maxNbChars = 200, $indexSize = 1),
            'checked'=> fake()->randomDigit(),
        ];
    }
}
