<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Poll;

class PollCreationTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_is_poll_created()
    {
        $this->assertDatabaseCount('polls', 100);
        $this->assertDatabaseCount('answers', 300);
    }
}
