<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Answer::orderBy('id')->cursorPaginate(15);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'proposition' => 'required|string',
            'checked' => 'required|regex:/[0-9]+/',
            'poll_id' => 'required|exists:polls,id'
        ]);

        $answer = new Answer($data);
        $answer->save();

        return response()->json([
            'message' => 'Answer created successfully',
            'data' => $answer
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function show(Answer $answer)
    {
        return response()->json([
            'data' => $answer
        ], 200);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Answer $answer)
    {
        
        $data = $request->validate([
        'proposition' => 'required|string',
        'checked' => 'required|regex:/[0-9]+/',
        'poll_id' => 'required|exists:polls,id'
        ]);

        $answer->update($data);

        return response()->json([
            'message' => 'Answer updated successfully',
            'data' => $answer,
        ], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Answer $answer)
    {
        $answer->delete();
        return response()->json([
            'message' => 'Answer deleted successfully'
        ], 200);
    }
}
