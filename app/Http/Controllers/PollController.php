<?php

namespace App\Http\Controllers;

use App\Models\Poll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Poll::with('answers')->orderBy('id')->cursorPaginate(15);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'subject' => 'required|string'
        ]);

        $poll = new Poll($data);
        $poll->save();

        return response()->json([
            'message' => 'Poll created successfully',
            'data' => $poll
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function show(Poll $poll)
    {
        return response()->json([
            'data' => $poll,
        ], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Poll $poll)
    {
        $data = $request->validate([
        'name' => 'required|string',
        'subject' => 'required|string',
        ]);

        $poll->update($data);

        return response()->json([
            'message' => 'Poll updated successfully',
            'data' => $poll,
        ], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function destroy(Poll $poll)
    {
        $poll->answers()->delete();
        $poll->delete();
        return response()->json([
            'message' => 'Poll and associated answers deleted successfully'
        ], 200);
    }
}
