<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    use HasFactory;

    protected $with = ['answers'];

    protected $fillable = [
        'name',
        'subject',
    ];

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
