<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;

    protected $fillable = [
        'proposition',
        'checked',
        'poll_id'
    ];

    public function poll()
    {
        return $this->belongsTo(Poll::class);
    }
}
